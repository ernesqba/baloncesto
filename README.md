# Proyecto Baloncesto
Este es un proyecto de ejemplo utilizado en la asignatura de CI/CD. El proyecto consiste en una aplicación web desarrollada con Java y Spring, que permite votar por el jugador preferido de la Liga ACB.

## Requisitos
Antes de comenzar, es necesario tener instalado lo siguiente:
* Docker
* Docker Compose
* Maven

## Instrucciones
Para levantar la aplicación, sigue estos pasos:
* Clona el repositorio con git clone https://gitlab.com/ernesqba/baloncesto.git
* Entra al directorio baloncesto con cd baloncesto
* Levanta el entorno de desarrollo con Docker Compose: docker-compose up
* Ejecutar una terminal en el contenedor de la image josehilera/ubuntu-ci (docker exec -it nombre-de-contenedor sh)
* Hacer la configuracion del runner de gitlab en nuestro entorno: gitlab-runner register
* Levantar el runner: gitlab-runner run
* Ejecutar el pipeline en GitLab
* Acceder a http://localhost:8080/Baloncesto para ver el proyecto
* Acceder a http://localhost:9000 para acceder a sonarqube