import java.io.*;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.*;
import javax.servlet.http.*;

import Jugador.Jugador;

public class ShowVotes extends HttpServlet {

    private ModeloDatos bd;

    @Override
    public void init(ServletConfig cfg) throws ServletException {
        bd = new ModeloDatos();
        bd.abrirConexion();
    }

    @Override
    public void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
        HttpSession s = req.getSession(true);
        List<String> nombres = new ArrayList<>();
        List<Integer> votos = new ArrayList<>();
        List<Jugador> jugadores = bd.cogerTodosLosJugadores();
        for (Jugador jugador: jugadores) {
            nombres.add(jugador.getNombre());
            votos.add(jugador.getVotos());
        }
        s.setAttribute("nombres", nombres);
        s.setAttribute("votos", votos);
        res.sendRedirect(res.encodeRedirectURL("VerVotos.jsp"));
    }

    @Override
    public void destroy() {
        bd.cerrarConexion();
        super.destroy();
    }
}
