import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import Jugador.Jugador;

public class ModeloDatos {

    private static final Logger LOGGER = Logger.getLogger("Baloncesto app");
    private Connection con;
    private Statement set;
    private ResultSet rs;

    public void abrirConexion() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            // Con variables de entorno
            String dbHost = System.getenv().get("DATABASE_HOST");
            String dbPort = System.getenv().get("DATABASE_PORT");
            String dbName = System.getenv().get("DATABASE_NAME");
            String dbUser = System.getenv().get("DATABASE_USER");
            String dbPass = System.getenv().get("DATABASE_PASS");

            String url = dbHost + ":" + dbPort + "/" + dbName;
            con = DriverManager.getConnection(url, dbUser, dbPass);

        } catch (Exception e) {
            // No se ha conectado
            LOGGER.severe("No se ha podido conectar");
            LOGGER.severe("El error es: " + e.getMessage());
        }
    }

    public List<Jugador> cogerTodosLosJugadores() {
        List<Jugador> jugadores = new ArrayList<>();

        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                int id = rs.getInt("id");
                String nombre = rs.getString("nombre");
                int votos = rs.getInt("votos");
                jugadores.add(new Jugador(id, nombre, votos));
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            LOGGER.severe("No lee de la tabla");
            LOGGER.severe("El error es: " + e.getMessage());
        }
        return jugadores;
    }

    public boolean existeJugador(String nombre) {
        boolean existe = false;
        String cad;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores");
            while (rs.next()) {
                cad = rs.getString("Nombre");
                cad = cad.trim();
                if (cad.compareTo(nombre.trim()) == 0) {
                    existe = true;
                }
            }
            rs.close();
            set.close();
        } catch (Exception e) {
            // No lee de la tabla
            LOGGER.severe("No lee de la tabla");
            LOGGER.severe("El error es: " + e.getMessage());
        }
        return (existe);
    }

    public void actualizarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=votos+1 WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            LOGGER.severe("No modifica la tabla");
            LOGGER.severe("El error es: " + e.getMessage());
        }
    }

    public Integer cogerVotosDeJugadorPorNombre(String nombre) {
        Integer votos = null;
        try {
            set = con.createStatement();
            rs = set.executeQuery("SELECT * FROM Jugadores WHERE nombre " + " LIKE '%" + nombre + "%'");
            rs.next();
            votos = rs.getInt("votos");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            LOGGER.severe("No modifica la tabla");
            LOGGER.severe("El error es: " + e.getMessage());
        }
        return votos;
    }

    public void reiniciarJgadores() {
        try {
            set = con.createStatement();
            set.executeUpdate("UPDATE Jugadores SET votos=0");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No modifica la tabla
            LOGGER.severe("No modifica la tabla");
            LOGGER.severe("El error es: " + e.getMessage());
        }
    }

    public void insertarJugador(String nombre) {
        try {
            set = con.createStatement();
            set.executeUpdate("INSERT INTO Jugadores " + " (nombre,votos) VALUES ('" + nombre + "',1)");
            rs.close();
            set.close();
        } catch (Exception e) {
            // No inserta en la tabla
            LOGGER.severe("No inserta en la tabla");
            LOGGER.severe("El error es: " + e.getMessage());
        }
    }

    public void cerrarConexion() {
        try {
            con.close();
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
    }

}
