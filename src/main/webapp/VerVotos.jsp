<!DOCTYPE html>
<html lang="es">
<head>
    <title>Votos de los jugadores</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <h1>Votos de los jugadores</h1>
    <table id="votesTable">
        <tr>
            <th>Nombre del jugador</th>
            <th>Votos</th>
        </tr>
        <%@ page import="java.util.List" %>
        <% for (int i = 0; i < ((List<String>) session.getAttribute("nombres")).size(); i++) { %>
        <tr>
            <td><%= ((List<String>) session.getAttribute("nombres")).get(i)%></td>
            <td><%= ((List<Integer>) session.getAttribute("votos")).get(i) %></td>
        </tr>
        <% } %>
    </table>
    <a href="index.html"> Ir al comienzo</a>
</body>
</html>