<!DOCTYPE html>
<html lang="es">
  <head>
    <title>Votacion mejor jugador liga ACB</title>
    <link href="estilos.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="resultado">
    <span style="font-size: 10px">
      Votaci&oacute;n al mejor jugador de la liga ACB
      <hr />
      <% String nombreP = (String) session.getAttribute("nombreCliente"); %>
      <br />Muchas gracias <%=nombreP%> por su voto
    </span>
    <br />
    <br />
    <a id="goIndexLink" href="index.html"> Ir al comienzo</a>
  </body>
</html>
