import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class ModeloDatosTest {

    @Test
    public void testExisteJugador() {
        System.out.println("Prueba de existeJugador");
        String nombre = "Rudy";
        ModeloDatos instance = new ModeloDatos();
        instance.abrirConexion();
        boolean expResult = true;
        boolean result = instance.existeJugador(nombre);
        assertEquals(expResult, result);
        instance.cerrarConexion();
    }

    @Test
    public void testActualizarJugador() {
        System.out.println("Prueba de actualizarJugador");
        String nombre = "Rudy";
        ModeloDatos instance = new ModeloDatos();
        instance.abrirConexion();
        Integer expResult = instance.cogerVotosDeJugadorPorNombre(nombre) + 1;
        instance.actualizarJugador(nombre);
        Integer result = instance.cogerVotosDeJugadorPorNombre(nombre);
        assertEquals(expResult, result);
        instance.cerrarConexion();
    }


}