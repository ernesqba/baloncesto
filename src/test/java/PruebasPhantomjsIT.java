import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

public class PruebasPhantomjsIT {
    private static WebDriver driver = null;

    @Test
    public void tituloIndexTest() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        assertEquals("Votacion mejor jugador liga ACB", driver.getTitle(),
                "El titulo no es correcto");
        System.out.println(driver.getTitle());
        driver.close();
        driver.quit();
    }

    @Test
    public void votarTest() {
        boolean result = false;

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);
        WebDriverWait wait = new WebDriverWait(driver, 10);
        
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        wait.until(ExpectedConditions.titleContains("Votacion mejor jugador liga ACB"));

        driver.findElement(By.id("visitorName")).sendKeys("test name");
        driver.findElement(By.id("visitorEmail")).sendKeys("test@email.com");
        driver.findElement(By.id("tavaresRadioButton")).click();
        driver.findElement(By.id("voteButton")).click();

        wait.until(ExpectedConditions.titleContains("Votacion mejor jugador liga ACB"));
        driver.findElement(By.id("goIndexLink")).click();

        wait.until(ExpectedConditions.titleContains("Votacion mejor jugador liga ACB"));
        driver.findElement(By.id("seeVotesButton")).click();
        
        WebElement table = driver.findElement(By.id("votesTable"));

        // Find all the rows in the table
        List<WebElement> rows = table.findElements(By.tagName("tr"));

        // Iterate through the rows and print the text of each cell
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            if(cells.size() != 0 && cells.get(0).getText().equals("Tavares") && cells.get(1).getText().equals("1")){
                result = true;
            }
        }
        assertEquals(true, result, "Logica de voto con error");
        driver.close();
        driver.quit();
    }

    @Test
    public void reiniciarVotosTest() {
        boolean result = true;

        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setJavascriptEnabled(true);
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, "/usr/bin/phantomjs");
        caps.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
                new String[] { "--web-security=no", "--ignore-ssl-errors=yes" });
        driver = new PhantomJSDriver(caps);

        WebDriverWait wait = new WebDriverWait(driver, 10);
        
        driver.navigate().to("http://localhost:8080/Baloncesto/");
        wait.until(ExpectedConditions.titleContains("Votacion mejor jugador liga ACB"));

        driver.findElement(By.id("resetVotesButton")).click();

        
        if (driver instanceof PhantomJSDriver) {
            PhantomJSDriver phantom = (PhantomJSDriver) driver;
            phantom.executeScript("window.alert = function(){}");
            phantom.executeScript("window.confirm = function(){return true;}");
        } else driver.switchTo().alert().accept();

        driver.findElement(By.id("seeVotesButton")).click();
        
        WebElement table = driver.findElement(By.id("votesTable"));

        // Find all the rows in the table
        List<WebElement> rows = table.findElements(By.tagName("tr"));

        // Iterate through the rows and print the text of each cell
        for (WebElement row : rows) {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            if(cells.size() != 0 && !cells.get(1).getText().equals("0")){
                result = false;
            }
        }
        assertEquals(true, result, "Logica de reiniciar votos con error");
        driver.close();
        driver.quit();
    }
}