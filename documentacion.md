# Introducción
Este proyecto consiste en una página web de votación para jugadores de la liga ACB desarrollada en Java con Spring. El proyecto incluye 3 funcionalidades: una pagina inicial donde se hace la votacion, una página adicional donde se pueden ver los votos actuales para cada jugador a través de una tabla y un botón para reiniciar los votos.

El uso de Integración y Entrega Continua (CI/CD) es importante en el desarrollo de software para garantizar que el proceso de entrega sea rápido, confiable y automatizado. En este proyecto se utilizará GitLab para llevar a cabo el proceso de CI/CD. GitLab no solo es una plataforma de desarrollo de software que funciona como un repositorio de Git, sino que también permite trabajar con metodologías como Scrum y Kanban, lo que facilita la organización del proyecto y la colaboración en equipo. Además, GitLab ofrece un registro de contenedores de imágenes Docker, lo que permite una mayor flexibilidad en el despliegue de la aplicación.

# Desarrollo
Para cumplir con los requisitos pedidos, se agregaron dos funcionalidades extra al proyecto de votación por jugadores de la liga ACB desarrollado en Java con Spring. La primera funcionalidad es una página adicional que permite ver los votos actuales para cada jugador de la liga. En esta página, los usuarios pueden ver una tabla que muestra la cantidad de votos que ha recibido cada jugador. Esto facilita la visualización de los votos actuales y motiva a los usuarios a seguir participando en la votación.

La segunda funcionalidad agregada es un botón para reiniciar los votos. Este botón es útil ya que permite reiniciar la votación en caso de ser necesario. Al hacer clic en este botón, se reinician los votos de todos los jugadores y se establecen en cero. De esta manera, se puede comenzar una nueva votación en caso de ser necesario.

Para la implementación de estas funcionalidades se utilizaron las milestone e issues de GitLab para gestionar el desarrollo y se agregaron pruebas e2e de PhantomJS para verificar la lógica de reinicio de votos y de votación por jugador. Además, se agregó una prueba unitaria para verificar que la funcionalidad de actualización del jugador funcionaba correctamente. Una revisión en profundidad de código fue realizada para dejar los major issues encontrados por SonarQube en cero.

Para llevar a cabo el proceso de Integración y Entrega Continua (CI/CD), se realizó un despliegue local de una imagen personalizada de Docker que contenía un gestor de base de datos MySQL, un servidor Tomcat y un runner de GitLab, el cual se configuró para conectarse con el repositorio de GitLab y ejecutar el pipeline localmente. Además, se incluyó un contenedor de SonarQube en la misma red virtual que el contenedor anterior que se encargó de realizar una revisión en profundidad del código.

El pipeline de GitLab incluye las siguientes etapas:
* unit-tests: se ejecutan las pruebas unitarias utilizando Maven.
* build: se empaqueta el proyecto utilizando Maven y se crea un artefacto.
* e2e-tests: se despliega la aplicación en un servidor Tomcat en un runner de gitlab local y se ejecutan pruebas e2e utilizando Maven.
* qa: se realiza una revisión de calidad del código utilizando SonarQube.
* release: se genera una imagen del proyecto y se sube a un registro de Docker en GitLab con tag:pre.
* deploy: se genera una imagen del proyecto y se sube a un registro de Docker en GitLab con tag:pre.

Destacar que el pipeline no terminará satisfactoriamente si los major issues encontrados por SonarQube superan el número de 20.

En resumen, se cumplieron los requisitos pedidos y se llevó a cabo un proceso de CI/CD eficiente y completo, utilizando herramientas como GitLab, Maven, Docker y SonarQube para asegurar la calidad del código y la entrega rápida y automatizada del proyecto.

# Conclusiones
En este proyecto se logró implementar una página web de votación para jugadores de la liga ACB desarrollada en Java con Spring. Además, se agregaron dos funcionalidades extra que mejoran la experiencia de usuario y facilitan la administración de la votación. La primera de ellas es una página adicional que permite visualizar los votos actuales para cada jugador de la liga, mientras que la segunda es un botón para reiniciar los votos.

La implementación del proyecto se llevó a cabo utilizando GitLab para el proceso de Integración y Entrega Continua (CI/CD). GitLab es una plataforma de desarrollo de software que se integra con metodologías como Scrum y Kanban, lo que facilita la organización del proyecto y la colaboración en equipo.

Se utilizaron herramientas como Maven, Docker y SonarQube para asegurar la calidad del código y la entrega rápida y automatizada del proyecto. Las pruebas automatizadas e2e y unitarias garantizaron el correcto funcionamiento del sistema y la revisión de código profunda realizada con SonarQube permitió detectar y corregir posibles problemas en el código.

En resumen, este proyecto demuestra la importancia de utilizar metodologías de Integración y Entrega Continua (CI/CD) y herramientas de desarrollo de software para garantizar un proceso de entrega rápido, confiable y automatizado. El uso de GitLab y otras herramientas permitió desarrollar un sistema eficiente y de calidad, satisfaciendo los requisitos pedidos y mejorando la experiencia de usuario.